import "./Picker.css";
import {EventEmitter} from "events";
import {v4 as uuidv4} from 'uuid';

interface IPickerOptions {
  elements?: string[];
  emit?: boolean;
  inputName?: string;
  labelClass?: string[] | string;
  render?: Function;
  icon?: boolean,
}

export class Picker extends EventEmitter {
  private readonly parent: Element;
  private readonly options: IPickerOptions = {
    emit: true,
    elements: ["#e6194b", "#3cb44b", "#ffe119", "#4363d8", "#f58231",
      "#911eb4", "#46f0f0", "#f032e6", "#bcf60c", "#fabebe",
      "#008080", "#e6beff", "#9a6324", "#fffac8", "#800000",
      "#aaffc3", "#808000", "#ffd8b1", "#000075", "#808080",
      "#ffffff", "#000000"],
    inputName: uuidv4(),
    labelClass: "picker-label",
    icon: false
  };
  private selected: string;
  
  constructor(parentSelector: string, options?: IPickerOptions) {
    super();
    this.parent = document.querySelector(parentSelector);
    if (options) {
      this.options = Object.assign(this.options, options);
    }
    
    this.generate();
  }
  
  getSelected() {
    return this.selected;
  }
  
  getOptions() {
    return this.options;
  }
  
  private generate() {
    let elements = this.options.elements;
    elements.forEach(this.render, this);
  }
  
  private render(element: string): void{
    const container = document.createElement('div');
    container.classList.add('picker-element');
  
    const input = document.createElement('input');
    input.classList.add('picker-input');
    input.id = `${this.options.inputName}-${element}`;
    input.type = 'radio';
    input.value = element;
    input.name = this.options.inputName;
    if (this.options.emit) {
      input.addEventListener('input', this.onClick.bind(this));
    }
  
    const label = document.createElement('label');
    if (Array.isArray(this.options.labelClass)) {
      label.classList.add(...this.options.labelClass);
    } else {
      label.classList.add(this.options.labelClass);
    }
    label.style.backgroundColor = element;
    label.htmlFor = `${this.options.inputName}-${element}`;
    if (this.options.icon) {
      label.innerText = element;
    }
    container.appendChild(input);
    container.appendChild(label);
    this.parent.appendChild(container);
  }
  
  private onClick(event: MouseEvent){
    this.selected = (event.target as HTMLElement).getAttribute('value');
    this.emit("change", {
      picker: this,
      input: event.target,
      selected: this.selected,
    });
  }
}
