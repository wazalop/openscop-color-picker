# *Openscop Color Picker*

a simple inline color picker for browser without jquery, with event emitting and form support.


## *installation*

for browser integration, copy openColorPicker.js in your server then import it in your html :

`<script src="openColorPicker.js"></script>`

or you can use npm :

`npm i openscop-color-picker`

## *Usage :*

see index.html in dist/ to see some examples.

the new Openscop.ColorPicker() is usable only if you take the bundle version in dist/ it's for use in browser, otherwise it will juste be ColorPicker()

You can edit the css or use a custom class if you want.

## *Options :*

you can initialise the picker with some options :

```typescript
new Openscop.ColorPicker(parentCssSelector: string, options?: IColorOptions);
```

```typescript
interface IColorPickerOptions {
  colors?: string[];
  maxColors?: number;
  emit?: boolean;
  inputName?: string;
  labelClass?: string;
}
```

only the parentCssSelector is mandatory, the options object have default value if you omit it :
```typescript
{
    emit: true,
    maxColors: 0,
    colors: ['#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231',
      '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe',
      '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000',
      '#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080',
      '#ffffff', '#000000'],
    inputName: 'color-picker-selector',
    labelClass: 'color-picker-label',
  };
```

* colors => an array of css colors
* emit => a simple boolean if you don't need the event emitter (if you will use it in a form for example, the eventEmitter is not a really good implementation for now)
* maxColors => you can limit the number of colors to show (it will take the x first)
* inputName => if you use multiple picker on the same page, it's on you to differentiate them with the input name attribute, it will also be use for id generation.
* labelClass => for customizing the class of the label so that you can customize it's appearance

# *dependancies*

it has only one dependancie, nodejs module events for the EventEmitter class. the dist is bundle with it already.

# *Licence*

no license attached, it's a fairly simple script use for a spécific case i make it public so that it can serve others, if you want to use it or modify it it's okay !
